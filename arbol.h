#ifndef ARBOL_H
#define ARBOL_H
#include <fstream>
#include <iostream>
using namespace std;

typedef struct _Nodo {
    int ingreso;
    struct _Nodo *izq;
    struct _Nodo *der;
} Nodo;

Nodo *arbol = NULL;
/* 
 * crea un nuevo nodo.
 */
Nodo *crearNodo(int dato) {
    Nodo *q;

    q = new Nodo();
    q->izq = NULL;
    q->der = NULL;
    q->ingreso = dato;
    return q;
}

class Arbol {
	private:
		Nodo *raiz = NULL;
		Nodo *ultimo = NULL;

	public:
		/* constructor*/
		Arbol();
        
		/* inserta un nuevo nodo*/
		void insertar_nodo (Nodo*&, int numero);
		void Preorden(Nodo *arbol);
		void Inorden(Nodo *arbol);
		void Postorden(Nodo *arbol);


};
#endif
