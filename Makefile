refix=/usr/local
CC = g++

CFLAGS = -g -Wall 
SRC = arbol.cpp programa.cpp
OBJ = arbol.o programa.o
APP = programa

all: $(OBJ)
	$(CC) $(CFLAGS)-o $(APP) $(OBJ) 

clean:
	$(RM) $(OBJ) $(APP)

install: $(APP)
	install -m 0755 $(APP) $(prefix)/bin

uninstall: $(APP)
	$(RM) $(prefix)/bin/$(APP)

.PHONY: install	
