#include <iostream>
#include <string>
using namespace std;
#include "arbol.h"


Arbol::Arbol() {}

void Arbol::insertar_nodo(Nodo*&, int numero) {//
	if(arbol == NULL){
		Nodo *nuevo_nodo = crearNodo(numero);
		arbol = nuevo_nodo;
		}
	else{
		int valorRaiz = arbol->ingreso;
		//si el numero es menor a la raiz, va a la izquierda
		if(numero < valorRaiz){
			insertar_nodo(arbol->izq, numero);
			}
			//si es mayor, es enviado a la derecha
			else{
				insertar_nodo(arbol->der, numero);
				}
		}
}

//Recorrido preorden
void Arbol::Preorden(Nodo *arbol){
	
	if (arbol != NULL) {
		cout << arbol->ingreso << " ";
		Preorden(arbol->izq);
		Preorden(arbol->der);
		}
		
}

void Arbol::Inorden(Nodo *arbol){

	if (arbol != NULL){
		Inorden(arbol->izq);
		cout << arbol->ingreso << " ";
		Inorden(arbol->der);
		}
		
}

void Arbol::Postorden(Nodo *arbol){

	if (arbol != NULL){
		Postorden(arbol->izq);
		Postorden(arbol->der);
		cout << arbol->ingreso << " ";
		}
		
}
